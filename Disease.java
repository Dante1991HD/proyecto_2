import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Disease {
    private double probabilidadInfeccion;
    private int promedioPasos;
    private boolean enfermo;
    private int contador;
    private HashMap < String, Double > enfermedadBase = new HashMap < String, Double > ();
    private HashMap < String, Double > afeccion = new HashMap < String, Double > ();

    Disease(double probabilidadInfeccion, int promedioPasos) {

        this.probabilidadInfeccion = probabilidadInfeccion;
        this.promedioPasos = promedioPasos;
        this.enfermo = false;
        this.contador = 0;
        enfermedadBase.put("Cerebrovascular", (0.16 + 0.40) * new Random().nextFloat());
        enfermedadBase.put("Asma", (0.16 + 0.40) * new Random().nextFloat());
        enfermedadBase.put("Fibrosis", (0.16 + 0.40) * new Random().nextFloat());
        enfermedadBase.put("Hipertension", (0.16 + 0.40) * new Random().nextFloat());
        afeccion.put("Obesidad", (0.16 + 0.40) * new Random().nextFloat());
        afeccion.put("Desnutrición", (0.16 + 0.40) * new Random().nextFloat());
    }

    public boolean getEnfermo() {
        return enfermo;
    }
    public void setEnfermo(boolean enfermo) {
        this.enfermo = enfermo;
    }
    public int getContador() {
        return contador;
    }
    public void setContador(int contador) {
        this.contador += contador;
    }
    public int getPromedioPasos() {
        return promedioPasos;
    }
    public void setPromedioPasos(int promedioPasos) {
        this.promedioPasos = promedioPasos;
    }
    public double getProbabilidadInfeccion() {
        return this.probabilidadInfeccion;
    }
    public boolean infectado() {
        return new Random().nextFloat() <= this.probabilidadInfeccion;
    }
    public HashMap < String, Double > getEnfermedadBase() {
        HashMap < String, Double > cronicos = new HashMap < String, Double > ();
        ArrayList < String > keysAsArray = new ArrayList < String > (this.enfermedadBase.keySet());
        String keycronicos = keysAsArray.get(new Random().nextInt(keysAsArray.size()));
        cronicos.put(keycronicos, this.enfermedadBase.get(keycronicos));
        return cronicos;
    }
    public HashMap < String, Double > getAfeccion() {
        HashMap < String, Double > afeccion = new HashMap < String, Double > ();
        ArrayList < String > keysAsArray = new ArrayList < String > (this.afeccion.keySet());
        String keyafeccion = keysAsArray.get(new Random().nextInt(keysAsArray.size()));
        afeccion.put(keyafeccion, this.afeccion.get(keyafeccion));
        return afeccion;
    }
}
