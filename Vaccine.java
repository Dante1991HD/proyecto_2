public interface Vaccine {
    public float probMuerte(float riesgoAnterior);
    public boolean inmunidad();
}
