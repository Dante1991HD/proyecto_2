public class Vacuna implements Vaccine {
    private int idVacuna;
    private int steps;
    private int cantidadDosis;

    public int getIdVacuna() {
        return idVacuna;
    }

    public void setIdVacuna(int idVacuna) {
        this.idVacuna = idVacuna;
    }

    Vacuna(int idVacuna, int steps, int dosis) {
        this.idVacuna = idVacuna;
        this.steps = steps;
        this.cantidadDosis = dosis;
    }

    public float probMuerte(float riesgoAnterior) {
        float riesgo;
        if (this.idVacuna == 1) {
            riesgo = riesgoAnterior - 0.25 f;
            if (riesgo < 0) {
                riesgo = 0;
            }
        } else if (this.idVacuna == 2) {
            riesgo = 0;
        } else {
            riesgo = 0;
        }
        return riesgo;
    }

    public boolean inmunidad() {
        return this.idVacuna == 3;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps -= steps;
    }

    public int getCantidadDosis() {
        return cantidadDosis;
    }

    public void setCantidadDosis(int cantidadDosis) {
        this.cantidadDosis -= cantidadDosis;
    }
}
