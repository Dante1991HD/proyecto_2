import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
public class Simulator {

    private ArrayList < Citizen > comunidad = new ArrayList < Citizen > ();
    private Comunity com;
    private Vacunacion planVacunacion;

    public void creaComunidad(Comunity com) {
        this.planVacunacion = new Vacunacion();
        this.com = com;
        this.com.creaComunidad(com, comunidad);
        this.com.riesgo(comunidad);
        Collections.sort(comunidad, new Comparator < Citizen > () {
            public int compare(Citizen c1, Citizen c2) {
                return Integer.valueOf((int)(c2.getRiesgo() * 100)).compareTo((int)(c1.getRiesgo() * 100));
            }
        });
    }

    public void run(int runs) {
        int maximoInfectados[] = {
            0,
            0
        };
        this.planVacunacion.recibeVacunas(com.getNumeroCiudadanos(), runs, runs);
        System.out.println("Cuadro resumen inicial");
        System.out.println("Total de la población: " + this.com.getNumeroCiudadanos());
        System.out.println("Poblacion con enfermedad base y/o afección: " + this.com.getPoblacionEbaseAfeccion(this.comunidad));
        System.out.println("promedio edad: " + this.com.getPromedioEdad(this.comunidad));
        System.out.println("Cantidad de vacunas: " + this.planVacunacion.getVacunasDisponibles());

        for (int i = 0; i < runs; i++) {

            HashSet < Citizen > graves = new HashSet < Citizen > ();
            ArrayList < Citizen > enfermos = new ArrayList < Citizen > ();
            ArrayList < Citizen > recuperados = new ArrayList < Citizen > ();
            HashSet < Citizen > muertos = new HashSet < Citizen > ();
            ArrayList < Citizen > sanos = new ArrayList < Citizen > ();
            this.planVacunacion.vacunar(comunidad);
            //	    	System.out.println("Cantidad de personas " + com.getNumeroCiudadanos() + " contagiados");
            //	    	System.out.println("Vac1 "+this.planVacunacion.getVacunadosVac1().size() + " vac2 " + planVacunacion.getVacunadosVac2().size() + " Vac3 "+ planVacunacion.getVacunadosVac3().size());

            for (Citizen ciudadano: comunidad) {
                if (ciudadano.isEnfermo()) {
                    if (new Random().nextFloat() <= ciudadano.getRiesgo()) {
                        graves.add(ciudadano);
                    }
                    enfermos.add(ciudadano);
                } else if (ciudadano.isEstado() && recuperados.contains(ciudadano) == false) {
                    if (new Random().nextFloat() <= ciudadano.getRiesgo()) {
                        ciudadano.setVivo(false);
                    }
                    if (ciudadano.isVivo()) {
                        recuperados.add(ciudadano);
                    }
                } else {
                    sanos.add(ciudadano);
                }
                if (ciudadano.isVivo() == false) {
                    muertos.add(ciudadano);
                }
            }
            if (maximoInfectados[0] < enfermos.size()) {
                maximoInfectados[0] = enfermos.size();
                maximoInfectados[1] = i;
            }
            com.setNumeroInfectados(enfermos.size());
            System.out.print("Paso: " + i + " existen: " + enfermos.size() + " infectados," + " graves: " + graves.size());
            System.out.print(" el número de recuperados es: " + recuperados.size() + " de muertos es " + muertos.size());
            System.out.println(" la cantidad de personas sin contagiarse es de: " + sanos.size());

            for (Citizen ciudadano: enfermos) {
                ciudadano.tratamiento();
                ciudadano.isEstado();
                ciudadano.contagio(this.comunidad, graves, ciudadano);
            }
            if (i == runs - 1) {
                System.out.println("Cuadro resumen Final");
                System.out.println("Total de la población viva: " + (recuperados.size() + sanos.size()));
                System.out.println("Población viva con enfermedad base y/o afección: " + (this.com.getPoblacionEbaseAfeccion(recuperados) + this.com.getPoblacionEbaseAfeccion(sanos)));
                System.out.println("Cantidad de contagios totales en la simulación: " + (com.getNumeroCiudadanos() - sanos.size()));
                System.out.println("Cantidad de inmunes y/o recuperados totales en la simulación: " + (recuperados.size() + this.planVacunacion.getVacunadosVac3().size()));
                System.out.println("Cantidad de fallecidos totales en la simulación: " + muertos.size());
                System.out.println("Promedio edad de la población viva: " + (this.com.getPromedioEdad(recuperados) + this.com.getPromedioEdad(sanos)) / 2);
                int totalVacunados = planVacunacion.getVacunadosVac1().size() + planVacunacion.getVacunadosVac2().size() + planVacunacion.getVacunadosVac3().size();
                System.out.println("El total de vacunados es: " + totalVacunados);
                System.out.println("El pick de contagios fue de: " + maximoInfectados[0] + " En el ciclo: " + maximoInfectados[1]);
            }
        }
    }
}
