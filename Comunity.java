import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
public class Comunity {
    private int numeroCiudadanos;
    private int promedioConeccionesFisicas;
    private Disease enfermedad;
    private int numeroInfectados;
    private double probabilidadConeccionesFisicas;
    private float promedioEdad;
    private int poblacionEbaseAfeccion;

    Comunity(int numeroCiudadanos, int promedioConeccionesFisicas, Disease enfermedad, int numeroInfectados, double probabilidadConeccionesFisicas) {
        this.numeroCiudadanos = numeroCiudadanos;
        this.promedioConeccionesFisicas = promedioConeccionesFisicas;
        this.enfermedad = enfermedad;
        this.numeroInfectados = numeroInfectados;
        this.probabilidadConeccionesFisicas = probabilidadConeccionesFisicas;
    }

    Comunity() {}
    public int getNumeroCiudadanos() {
        return numeroCiudadanos;
    }
    public int getPromedioConeccionesFisicas() {
        return new Random().nextInt(this.promedioConeccionesFisicas);
    }
    public boolean coneccionFisica() {
        return new Random().nextDouble() <= this.probabilidadConeccionesFisicas;
    }
    public Disease getEnfermedad() {
        return enfermedad;
    }
    public int getNumeroInfectados() {
        return numeroInfectados;
    }
    public void setNumeroInfectados(int numeroInfectados) {
        this.numeroInfectados = numeroInfectados;
    }
    public int[] ciudadanosAleatorios(ArrayList < Citizen > comunidad, int cantidadCiudadanos) {
        int[] idUnico = new int[cantidadCiudadanos];
        for (int i = 0; i < cantidadCiudadanos; i++) {
            Citizen ciudadanoAleatorio = comunidad.get(new Random().nextInt(comunidad.size()));
            idUnico[i] = ciudadanoAleatorio.getId();
        }
        return idUnico;
    }
    public void creaComunidad(Comunity com, ArrayList < Citizen > comunidad) {

        for (int i = 0; i < com.getNumeroCiudadanos(); i++) {
            comunidad.add(new Citizen(i, com));
        }
        Collections.shuffle(comunidad);
        for (int i = 0; i < com.getNumeroInfectados(); i++) {
            comunidad.get(i).setEnfermo(true);
        }
        Collections.shuffle(comunidad);
        for (int i = 0; i < (int)(com.numeroCiudadanos * 0.25); i++) {
            comunidad.get(i).setEnfermedadBase(this.enfermedad.getEnfermedadBase());
        }
        Collections.shuffle(comunidad);
        for (int i = 0; i < (int)(com.numeroCiudadanos * 0.65); i++) {
            comunidad.get(i).setAfeccion(this.enfermedad.getAfeccion());
        }
    }
    public void riesgo(ArrayList < Citizen > comunidad) {
        for (Citizen ciudadano: comunidad) {
            float riesgo = 0;
            if (50 > ciudadano.getEdad()) {
                riesgo = 0.002 f;
            } else if (70 > ciudadano.getEdad() && ciudadano.getEdad() > 50) {
                riesgo = 0.036 f;
            } else if (80 > ciudadano.getEdad() && ciudadano.getEdad() > 70) {
                riesgo = 0.08 f;
            } else if (ciudadano.getEdad() > 80) {
                riesgo = 0.146 f;
            }
            if (!ciudadano.getEnfermedadBase().containsKey(null)) {
                riesgo += ciudadano.getEnfermedadBase().get(ciudadano.getEnfermedadBase().keySet().toArray()[0]);
            }
            if (!ciudadano.getAfeccion().containsKey(null)) {
                riesgo += ciudadano.getAfeccion().get(ciudadano.getAfeccion().keySet().toArray()[0]);
            }
            ciudadano.setRiesgo(riesgo);
            //		   System.out.print("El cuidadano tiene " + ciudadano.getAfeccion());
            //		   System.out.print(" ademas de " + ciudadano.getEnfermedadBase());
            //		   System.out.println(" su edad es: " + ciudadano.getEdad() + " por lo tanto su ratio de riesgo " + ciudadano.getRiesgo());
        }
    }

    public float getPromedioEdad(ArrayList < Citizen > comunidad) {
        this.promedioEdad = 0;
        for (Citizen ciudadano: comunidad) {
            this.promedioEdad += ciudadano.getEdad();
        }
        this.promedioEdad /= comunidad.size();

        return promedioEdad;
    }

    public int getPoblacionEbaseAfeccion(ArrayList < Citizen > comunidad) {
        this.poblacionEbaseAfeccion = 0;
        for (Citizen ciudadano: comunidad) {
            if (!ciudadano.getEnfermedadBase().containsKey(null) || !ciudadano.getAfeccion().containsKey(null)) {
                this.poblacionEbaseAfeccion++;
            }
        }
        return this.poblacionEbaseAfeccion;
    }
}
