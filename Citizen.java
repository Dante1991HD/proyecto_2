import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class Citizen {

    private Comunity comunidad;
    private int id;
    private int periodo = 0;
    private Disease enfermedad;
    private boolean estado = false;
    private boolean vivo = true;
    private boolean inmunidad = false;
    private ArrayList < Vacuna > sistInmunologico = new ArrayList < Vacuna > ();;
    private HashMap < String, Double > enfermedadBase = new HashMap < String, Double > ();
    private HashMap < String, Double > afeccion = new HashMap < String, Double > ();
    private int edad;
    private float riesgo;

    Citizen(int id, Comunity com) {
        this.id = id;
        this.comunidad = com;
        this.enfermedad = new Disease(com.getEnfermedad().getProbabilidadInfeccion(), com.getEnfermedad().getPromedioPasos());
        this.setEdad(new Random().nextInt(100));
        this.enfermedadBase.put(null, null);
        this.afeccion.put(null, null);
    }

    public boolean isEstado() {
        if (this.enfermedad.getContador() == this.enfermedad.getPromedioPasos()) {
            this.setEstado(true);
            this.setEnfermo(false);
        }
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }
    public boolean isEnfermo() {
        return this.enfermedad.getEnfermo();
    }
    public void setEnfermo(boolean enfermo) {
        this.enfermedad.setEnfermo(enfermo);
    }
    public void setContador(int contador) {
        this.enfermedad.setContador(contador);
    }
    public void tratamiento() {
        if (this.isEnfermo()) {
            this.enfermedad.setContador(1);
        }
    }

    public void contagio(ArrayList < Citizen > comunidad, HashSet < Citizen > graves, Citizen ciudadano) {

        int contactos = this.comunidad.getPromedioConeccionesFisicas();
        int[] ciudadanosAleatorios = this.comunidad.ciudadanosAleatorios(comunidad, contactos);
        if (this.inmunidad == false && graves.contains(ciudadano) == false) {
            for (int i = 0; i < contactos; i++) {
                if (comunidad.get(ciudadanosAleatorios[i]).isEstado() == false) {
                    if (this.comunidad.coneccionFisica() && this.enfermedad.infectado() && comunidad.get(ciudadanosAleatorios[i]).isInmunidad() == false) {
                        //Cambiar orden (Cambiado)
                        comunidad.get(ciudadanosAleatorios[i]).setEnfermo(true);
                    }
                }
            }
        }
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public HashMap < String, Double > getEnfermedadBase() {
        return enfermedadBase;
    }

    public void setEnfermedadBase(HashMap < String, Double > enfermedadBase) {
        this.enfermedadBase = enfermedadBase;
    }
    public HashMap < String, Double > getAfeccion() {
        return afeccion;
    }

    public void setAfeccion(HashMap < String, Double > afeccion) {
        this.afeccion = afeccion;
    }

    public boolean isInmunidad() {
        return inmunidad;
    }

    public void setInmunidad(boolean inmunidad) {
        this.inmunidad = inmunidad;
    }

    public float getRiesgo() {
        return riesgo;
    }

    public void setRiesgo(float riesgo) {
        this.riesgo = riesgo;
    }
    public void setPeriodo(int periodo) {
        this.periodo += periodo;
    }
    public int getPeriodo() {
        return periodo;
    }

    public ArrayList < Vacuna > getSistInmunologico() {
        return this.sistInmunologico;
    }

    public void setSistInmunologico(Vacuna remove) {
        this.sistInmunologico.add(remove);

    }

    public boolean isVivo() {
        return vivo;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }
}
