import java.util.ArrayList;
import java.util.HashSet;

public class Vacunacion {

    public HashSet < Citizen > getVacunadosVac1() {
        return vacunadosVac1;
    }
    public void setVacunadosVac1(HashSet < Citizen > vacunadosVac1) {
        this.vacunadosVac1 = vacunadosVac1;
    }
    public HashSet < Citizen > getVacunadosVac2() {
        return vacunadosVac2;
    }
    public void setVacunadosVac2(HashSet < Citizen > vacunadosVac2) {
        this.vacunadosVac2 = vacunadosVac2;
    }
    public HashSet < Citizen > getVacunadosVac3() {
        return vacunadosVac3;
    }
    public void setVacunadosVac3(HashSet < Citizen > vacunadosVac3) {
        this.vacunadosVac3 = vacunadosVac3;
    }
    private HashSet < Citizen > esquemaIncompleto = new HashSet < Citizen > ();
    private HashSet < Citizen > vacunadosVac1 = new HashSet < Citizen > ();
    private HashSet < Citizen > vacunadosVac2 = new HashSet < Citizen > ();
    private HashSet < Citizen > vacunadosVac3 = new HashSet < Citizen > ();
    private ArrayList < Vacuna > vacuna1 = new ArrayList < Vacuna > ();
    private ArrayList < Vacuna > vacuna2 = new ArrayList < Vacuna > ();
    private ArrayList < Vacuna > vacuna3 = new ArrayList < Vacuna > ();
    private int vacunasDisponibles;

    public void vacunar(ArrayList < Citizen > comunidad) {
        //    	System.out.println(vacuna1.size() + " " + vacuna2.size() + " " + vacuna3.size());
        for (Citizen persona: comunidad) {
            if (esquemaIncompleto.contains(persona)) {
                if (persona.getSistInmunologico().get(0).getSteps() == 1) {
                    if (persona.getSistInmunologico().get(0).getIdVacuna() == 2) {
                        vacunadosVac2.add(persona);
                        esquemaIncompleto.remove(persona);
                    } else {
                        vacunadosVac3.add(persona);
                        esquemaIncompleto.remove(persona);
                    }
                    persona.setInmunidad(persona.getSistInmunologico().get(0).inmunidad());
                    persona.setRiesgo(persona.getSistInmunologico().get(0).probMuerte(0));
                } else {
                    persona.getSistInmunologico().get(0).setSteps(1);
                    //    				System.out.println("Step " + persona.getSistInmunologico().get(0).getSteps());			
                }
            } else if (vacunadosVac1.contains(persona) == false && vacunadosVac2.contains(persona) == false && vacunadosVac3.contains(persona) == false) {
                if (this.vacuna2.size() > 0) {
                    //        		System.out.println(this.vacuna2.size());
                    persona.setSistInmunologico(this.vacuna2.remove(0));
                    persona.getSistInmunologico().get(0).setCantidadDosis(1);
                    esquemaIncompleto.add(persona);
                } else if (this.vacuna3.size() > 0) {
                    //        		System.out.println(this.vacuna3.size());
                    persona.setSistInmunologico(this.vacuna3.remove(0));
                    persona.getSistInmunologico().get(0).setCantidadDosis(1);
                    esquemaIncompleto.add(persona);
                } else if (this.vacuna1.size() > 0) {
                    //        		System.out.println(this.vacuna1.size());
                    vacunadosVac1.add(persona);
                    persona.setSistInmunologico(this.vacuna1.remove(0));
                    persona.setInmunidad(persona.getSistInmunologico().get(0).inmunidad());
                    persona.setRiesgo(persona.getSistInmunologico().get(0).probMuerte(0));
                } else {
                    break;
                }
            }
        }
    }
    public void recibeVacunas(int tamanioComunidad, int runs, int progresion) {
        int vacunasRecibidas = Math.round(tamanioComunidad / 2);
        for (int i = 0; i < Math.round(vacunasRecibidas * 3 / 6); i++) {
            this.vacuna1.add(new Vacuna(1, 0, 1));
        }
        for (int i = 0; i < Math.round(vacunasRecibidas * 2 / 6); i++) {
            this.vacuna2.add(new Vacuna(2, 3, 2));
        }
        for (int i = 0; i < Math.round(vacunasRecibidas * 1 / 6); i++) {
            this.vacuna3.add(new Vacuna(3, 6, 2));
        }
    }
    public int getVacunasDisponibles() {
        return vacunasDisponibles;
    }
    public void setVacunasDisponibles(int vacunasDisponibles) {
        this.vacunasDisponibles = vacunasDisponibles;
    }
}
